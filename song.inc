
RODATA_SEGMENT

_D := 26
_E := 23
_G := 19
_A := 17
__ :=  0

songdata:
   .byte _E,_G,_E,_D,_E,__,_E,_D,_E,_G,_E,_D,_E,__,__,__
   .byte _E,_A,_E,_D,_E,__,_G,_E,_D,_E,__,_E,__,__,_E,_E

CODE_SEGMENT

song:
   ldy   frmcnt
   tya
   rol
   rol
   rol
   and   #$03
   tax
   lda   songdata+16,x
   sta   AUDF1
   lda   #$0c
   sta   AUDC1
   lda   #$03
   sta   AUDV1
   tya
   lsr
   lsr
   lsr
   tax
   lda   songdata,x
   beq   @notone
   sta   AUDF0
   lda   #$04
   sta   AUDC0
   tya
   and   #$07
   eor   #$07
@notone:
   asl
   sta   AUDV0

