
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

uint8_t buffer[8448];
uint8_t *header  = &buffer[0x2000];
uint8_t *pages   = &buffer[0x2010];
uint8_t *chksums = &buffer[0x2040];


uint8_t checksum( uint8_t *data, uint16_t count )
{
   uint8_t sum = 0;
   uint16_t i;

   for( i = 0; i < count; ++i )
   {
      sum += data[i];
   }

   return sum;
}


void fixheader()
{
   uint8_t sum = checksum( header, 8 );
   
   header[4] += 0x55 - sum;
}

int open_rom( const char *filename )
{
   int fd;
   off_t filesize;
   fd = open( filename, O_RDWR );
   // TODO: better check if open failed
   if( fd < 0 )
   {
      fprintf( stderr, "Open failed: %s\n", strerror( errno ) );
      return -1;
   }

   filesize = lseek( fd, 0, SEEK_END );
   if( filesize % 0x2100 )
   {
      fprintf( stderr, "This does not seem to be a SuperCharger image.\n" );
      return -1;
   }
   printf( "file contains %ld images.\n", filesize / 0x2100 );
   return fd;
}

int load_rom( int fd, int image )
{
   if( lseek( fd, image * 0x2100, SEEK_SET ) != image * 0x2100 )
   {
      fprintf( stderr, "seek to image %d failed.\n", image );
      return -1;
   }
   if( read( fd, &buffer[0], 0x2100 ) != 0x2100 )
   {
      fprintf( stderr, "reading image %d failed.\n", image );
      return -1;
   }
   return 0;
}

int save_header( int fd, int image )
{
   if( lseek( fd, image * 0x2100 + 0x2000, SEEK_SET ) != image * 0x2100 + 0x2000 )
   {
      fprintf( stderr, "seek to tapeheader of image %d failed.\n", image );
      return -1;
   }
   if( write( fd, header, 0x08 ) != 0x08 )
   {
      fprintf( stderr, "writing tapeheader of image %d failed.\n", image );
      return -1;
   }
   if( lseek( fd, image * 0x2100 + 0x2040, SEEK_SET ) != image * 0x2100 + 0x2040 )
   {
      fprintf( stderr, "seek to checksums of image %d failed.\n", image );
      return -1;
   }
   if( write( fd, chksums, 0x18 ) != 0x18 )
   {
      fprintf( stderr, "writing checksums of image %d failed.\n", image );
      return -1;
   }
   return 0;
}

int main( int argc, char *argv[] )
{
   int fd;
   int i;
   uint8_t sum;

   if( (argc != 2) || (!strcmp( argv[1], "-h" )) )
   {
      fprintf( argc != 2 ? stderr : stdout, "usage: %s <name_of_rom_image>\n", argv[0] );
      return( argc != 2 ? 1 : 0 );
   }

   fd = open_rom( argv[1] );
   if( fd < 0 )
   {
      return 5;
   }
   if( load_rom( fd, 0 ) )
   {
      fprintf( stderr, "error reading rom data\n" );
      close( fd );
      return 5;
   }

   sum = checksum( header, 8 );
   header[4] += 0x55 - sum;
   for( i = 0; i < 0x18; ++i )
   {
      sum = checksum( &buffer[0x100*i], 0x100 ) + pages[i];
      chksums[i] = 0x55 - sum;
   }

   if( save_header( fd, 0 ) )
   {
      fprintf( stderr, "error writing rom data\n" );
      close( fd );
      return 5;
   }
   close( fd );

   return 0;
}

