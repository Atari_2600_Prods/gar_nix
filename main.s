
.include "vcs.inc"
.include "globals.inc"

.ifndef ARSC
.define ARSC 0
.endif

.if ARSC
.define RODATA_SEGMENT .segment "RODATA3"
.define CODE_SEGMENT   .segment "CODE3"
.else
.define RODATA_SEGMENT .segment "RODATA"
.define CODE_SEGMENT   .segment "CODE"
.segment "VECTORS"
; nmi removed, because it will never happen on an 6507
.addr reset ; RESET
.addr waitoverscan + 8 ; IRQ: will only occur with brk and crash
.endif

.segment "ZEROPAGE"

coderam:
   .res $6c ; ramcodeend-ramcode-2
tcol:
   .word $0000
frmcnt:
   .byte 0
revcnt:
   .byte 0
tcolst:
   .byte 0
tmp8:
   .word 0

value:
   .word $0000
delta:
   .word $0000

RODATA_SEGMENT

addrs:
   .byte <coderam+14,<coderam+19,<coderam+24,<coderam+29
   .byte <coderam+34,<coderam+39,<coderam+44,<coderam+49
addrsend:

logo1:
   .byte $cd,$cd,$79,$31,$31,$79,$cd,$cd
logo2:
   .byte $63,$67,$6d,$cd,$df,$9f,$b1,$b1
.if 0
txt1:
   .byte $00,$4a,$4a,$7b,$4a,$49,$00,$3d,$21,$39,$21,$3d ;,$00
txt2:
   .byte $00,$4a,$4a,$6b,$5a,$49,$00,$28,$28,$ea,$2d,$e8 ;,$00
txt3:
   .byte $00,$e7,$94,$97,$94,$e7,$00,$08,$08,$08,$88,$3e ;,$00
txt4:
   .byte $00,$1d,$24,$24,$24,$1d,$00,$04,$04,$04,$0a,$11 ;,$00
.else
txt1:
   .byte $00,$00,$00,$00,$00,$78,$cd,$cd,$dd,$c1,$cd,$78 ;,$00
txt2:
   .byte $00,$c0,$c0,$c0,$c0,$c0,$19,$d9,$df,$d9,$d9,$cf ;,$00
txt3:
   .byte $00,$37,$73,$f3,$b3,$33,$07,$30,$30,$e0,$30,$e0 ;,$00
txt4:
   .byte $00,$cd,$cc,$78,$30,$78,$cd,$cc,$00,$00,$00,$00 ;,$00
.endif
.if NTSC
   .byte $00
.else
highs:
   .byte $00,$20,$20,$40,$60,$80,$A0,$C0
   .byte $D0,$B0,$90,$70,$50,$30,$30,$20
.endif

CODE_SEGMENT

waitvblank:
   lda   #TIMER_SCREEN
   .byte $2c
waitscreen:
   lda   #TIMER_OVERSCAN
waitvblanka:
waitscreena:
@waitloop:
   bit   TIMINT
   bpl   @waitloop
   lsr
   sta   WSYNC
   sta   TIM64TI
   bcs   @no1k
   sta   TIM1KTI
@no1k:
   rol
   asl
   sta   VBLANK
   rts

reset:
   cld
   lda   #$00
:
   tsx
   pha
   bne   :-          ; SP=$FF, X = A = 0

.if ARSC
.include "gentables_arsc.inc"
.else
.include "gentables.inc"
.endif

waitoverscan:
   lda   #TIMER_VBLANK
waitoverscana:
   jsr   waitscreena

   lda   #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta   WSYNC
   sta   VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne   @syncloop   ; branch until VSYNC has been reset

   dec   revcnt

   ldy   #<(addrsend-addrs-1)
   clc
@loop:
   lda   sotr,y
   adc   frmcnt
   tax
   lda   siner,x
   ldx   addrs,y
   sta   $00,x
   dey
   bpl   @loop
   eor   #$80
   sta   tcolst

   jsr   waitvblank

.if NTSC
   ldx   #$01
   stx   CTRLPF
   dex
.else
   ldx   #$10
:
   sta   WSYNC
   stx   CTRLPF      ; last write is #$01
   dex

   bne   :-
.endif

   jsr   coderam
   
   stx   COLUBK

   jsr   waitscreen

.include "song.inc"
   ; Y contains frmcnt

.if 1
   lda   blackr-$10,y
   sta   COLUPF
.endif
   
   inc   frmcnt

   bne   :++
   ldx   #$00
:
   sta   sotw,x
   inx
   adc   #$20
;   cpx   #$08
   bcc   :-
:

   bvc   waitoverscan
;   jmp   waitoverscan

ramcode:
   sta   WSYNC
@loop:
   lda   logo1r,x ; 4= 4
   sta   PF1      ; 3= 7
   lda   logo2r,x ; 4=11
   sta   PF2|$100 ; 3=14

   lda   $f120,x  ; 4=26
   sta   COLUBK   ; 3=29
   lda   $f128,x  ; 4=33
   sta   COLUBK   ; 3=36
   lda   $f130,x  ; 4=40
   sta   COLUBK   ; 3=43
   lda   $f138,x  ; 4=47
   sta   COLUBK   ; 3=50
   lda   $f140,x  ; 4=54
   sta   COLUBK   ; 3=57
   lda   $f148,x  ; 4=61
   sta   COLUBK   ; 3=64
   lda   $f150,x  ; 4=67
   sta   COLUBK   ; 3=71
   lda   $f158,x  ; 4=67
   sta   COLUBK   ; 3=71

   inx            ; 2=73
   bpl   @loop    ; 3=76

   ldy   tcolst
   lda   (tcol),y
   sta   COLUBK
   ldx   #<(txt2-txt1+1)
@loop2:
   lda   #$08
   sta   tmp8
@loop1:
   iny
   sta   WSYNC
   jsr   sleep12  ; 12=12
   lda   (tcol),y ; 5=17
   sta   COLUBK   ; 3=20

   lda   txt1-1,x ; 4=24
   sta   PF1      ; 3=27
   lda   txt2-1,x ; 4=31
   sta   PF2      ; 3=34
   lda   txt4-1,x ; 4=38
   sta   PF1      ; 3=41
   lda   txt3-1,x ; 4=45
   sta   PF2      ; 3=48
   dec   tmp8
   bne   @loop1
   dex
   bne   @loop2
   sta   WSYNC
sleep12:
   rts
   .word colr ; initial value for tcol
ramcodeend:
