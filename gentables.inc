
;   ldx   #$00
@colloop:
   lda   #$00
   sta   logo1w,x
.if 1
   sta   blackw,x
.endif
   txa
.if NTSC
   and   #$f0
   sta   tmp8
.else
   lsr
   lsr
   lsr
   lsr
   tay
.endif
   txa
   and   #$0f
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
.if NTSC
   ora   tmp8
.else
   ora   highs,y
.endif
   sta   colw,x

   inx
   bne   @colloop

; sine table

   ldy   #$3f

; Accumulate the delta (normal 16-bit addition)
@sinloop:
   clc
   lda   delta+0
   adc   value+0
   sta   value+0
   lda   delta+1
   adc   value+1
   sta   value+1

; Reflect the value around for a sine wave
   sta   sinew+$c0,x
   sta   sinew+$80,y
   eor   #$7f
   sta   sinew+$40,x
   sta   sinew+$00,y

; Increase the delta, which creates the "acceleration" for a parabola
   lda   #$08
   adc   delta+0   ; this value adds up to the proper amplitude
   sta   delta+0
   bcc   :+
   inc   delta+1
:
   inx
   dey
   bpl   @sinloop

   ldx   #$7f
   ldy   #$0b
@imgloop:

; Compact code: hijack loop for copying code
   cpx   #<(ramcodeend-ramcode)
   bcs   :+
   lda   ramcode,x
   sta   coderam,x
:

   cpy   #$08
   bcs   :+
   lda   logo1,y
   sta   logo1w,x
   lda   logo2,y
   sta   logo2w,x
:
   txa
   and   #$07
   bne   :+
   dey
:
   dex
   bpl   @imgloop
