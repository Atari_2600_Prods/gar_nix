
.ifndef NTSC
.define NTSC 0
.endif

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
; values are calculated by 2*timer value | timer index (0: TIM1KTI, 1: TIM64TI)
.if NTSC
.define TIMER_VBLANK   1+2*$10 ; ~2688 cycles
.define TIMER_SCREEN   0+2*$10
.define TIMER_OVERSCAN 1+2*$0f ; ~1280 cycles
.else
.define TIMER_VBLANK   1+2*$29 ; ~2688 cycles
.define TIMER_SCREEN   0+2*$13
.define TIMER_OVERSCAN 1+2*$17 ; ~1280 cycles
.endif

; all symbols visible to other source files

siner := $f000
colr  := $f100
blackr:= $f200
logo1r:= $f300
logo2r:= $f380
sotr  := $f2f0

colw  := colr   | $400
blackw:= blackr | $400
sinew := siner  | $400
logo1w:= logo1r | $400
logo2w:= logo2r | $400
sotw  := sotr   | $400

; 0pglobals.s
.globalzp frmcnt
.globalzp revcnt
.globalzp tcolst
.globalzp tcol
.globalzp tmp8
.globalzp coderam

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana

; gentables.s
.global   gentables

; gfx.s
.global   logo1
.global   logo2
.global   eh1
.global   eh2
.global   eh3
.global   eh4

; ramcode.s
.global   ramcode
.global   ramcodeend
.global   sleep12

; payload.s
.global   payload

; song.s
.global   song

; lfsr.s
.global   lfsr15

; setsins.s
.global   setsins

; text.s
.global   text

