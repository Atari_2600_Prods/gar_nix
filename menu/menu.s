
.include "vcs.inc"
.include "globals.inc"

.segment "ZEROPAGE"
temp        = localramstart + 0
dataheight  = localramstart + 1
coloroffset = localramstart + 2
colortab    = localramstart + 3 ; $40 bytes

.segment "GFXDATA"
.include "menugfx.inc"

.segment "RODATA"
brighttab:
   .byte $00,$02,$04,$06,$08,$0a,$0c,$0e
   .byte $0e,$0c,$0a,$08,$06,$04,$02,$00

.segment "CODE"
payload:

   lda   #$03
   sta   NUSIZ0
   sta   NUSIZ1
   sta   VDELP0
   sta   VDELP1

   ldx   #$07
   sta   WSYNC
   bit   $ea
:
   dex
   bne   :-
   sta   RESP0
   sta   RESP1
   lda   #$10
   sta   HMP0
   adc   #$10
   sta   HMP1
   inx
   stx   CTRLPF
   sta   WSYNC
   sta   HMOVE

   lda   #$ff
   sta   PF0
   sta   PF1
   dec   coloroffset
   ldy   coloroffset
   ldx   #$3f

   jsr   waitvblank

:
   txs
   tya
   lsr
   and   #$0f
   tax
   lda   brighttab,x
   sta   temp 
   tya
   and   #$e0
   lsr
   lsr
   lsr
   lsr
   lsr
   adc   #$02
   tax
   lda   ntscpaltable,x
   ora   temp 
   tsx
   sta   colortab,x
   iny
   dex
   bpl   :-

   sta   WSYNC

   ldx   coloroffset
   cpx   #$10
   bcs   :+
   lda   brighttab,x
   sta   COLUBK
:
   ldx   #$03
:
   sta   WSYNC
   dex
   bpl   :-

   txs

   lda   #$7f
   sta   dataheight

   sta   WSYNC
   ldx   #$0c
:
   dex
   bne   :-
@spriteloop:
   lda   dataheight  ; 3= 3
   lsr               ; 2= 5
   tay               ; 2= 7
   lda   colortab,y  ; 4=11
   sta   COLUP0      ; 3=14
   sta   COLUP1      ; 3=17

   lda   gfx0,y      ; 4=21
   sta   GRP0        ; 3=24
   lda   gfx1,y      ; 4=28
   sta   GRP1        ; 3=31
   lda   gfx2,y      ; 4=35
   sta   GRP0        ; 3=38
   lda   gfx3,y      ; 4=42
   sta   temp        ; 3=45
   ldx   gfx4,y      ; 4=49
   lda   gfx5,y      ; 4=53
   ldy   temp        ; 3=56
   sty   GRP1        ; 3=59
   stx   GRP0        ; 3=62
   sta   GRP1        ; 3=65
   stx   GRP0        ; 3=68
   dec   dataheight  ; 5=73
   bpl   @spriteloop ; 3=76

   ldx   #$04
:
   sta   WSYNC
   dex
   bne   :-
   stx   COLUBK

   jsr   waitscreen

   bit   SWCHA
   bvc   @pal
   bpl   @ntsc
   lda   SWCHB
   lsr
   bcs   @nontsc
@ntsc:
   jmp   ($fbfc)
@nontsc:
   lsr
   bcs   @nopal
@pal:
   jmp   ($f9fc)
@nopal:

   jmp   waitoverscan
