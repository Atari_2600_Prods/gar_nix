
; configuration parameters

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; NTSC values, determined by trial and error
; values are calculated by 2*timer value | timer index (0: TIM1KTI, 1: TIM64TI)
.define TIMER_VBLANK   1+2*$23 ; ~2688 cycles
.define TIMER_SCREEN   0+2*$10
.define TIMER_OVERSCAN 1+2*$12 ; ~1280 cycles

; all symbols visible to other source files

; 0pglobal.s
.globalzp schedule
.globalzp temp8
.globalzp localramstart

; payload.s
.global   payload

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana

; splash.s
.global   splash
.global   ntscpaltable

