
.include "vcs.inc"
.include "globals.inc"

.segment "RODATA"

ntscpaltable:
; ora table
  .byte $00,$20,$20,$40,$60,$80,$A0,$C0
  .byte $D0,$B0,$90,$70,$50,$30,$30,$20

.segment "ZEROPAGE"

frmcnt = $81

; gfx are written to $90-$ec

.segment "CODE"

splash:
   lda   frmcnt
   bne   @noinit
   ldy   #$03       ; initialize
   sty   CTRLPF
   dey
   sty   AUDC0
   ldx   #$1f       ; X on purpose, will be used in copy below
   stx   AUDF0      ; setup initial (motor) sound effect
   lda   #$fc       ; number of remaining frames for intro and volume
   sta   AUDV0
   sta   frmcnt

   lda   #%00110000 ; initial bit pattern for atari logo
@gfxloop:
   tay              ; also number of lines generated
   ora   #$80       ; add center bar
@loop1:
   sta   $71,x      ; copy to $90-$ec, X starts with 1, see above
   inx
   dey
   bne   @loop1     ; from here on Y=$00, assumed below
   and   #$7f       ; remove center bar
   lsr              ; shift playfield data (and height)
   cmp   #%00000001 ; as long as there's space left for gfx
   bne   @gfxloop

@noinit:
   ldy   #$00
   cmp   #$fb             ; on first frame
   bne   @notmagicframe   ; check for fire button on left joystick
   bit   INPT4            ; do nothing until released
   bpl   @waitbuttonup    ; (used for getting sync on recording)
@notmagicframe:
   dec   frmcnt
@waitbuttonup:
   jsr   waitvblank
   
   ldx   frmcnt           ; any frames left
   cli                  ; no: skip to next part
   beq   @exit            ; and don't draw anything
   sei
@notdone:
   cpx   #$10
   bcs   @nomute          ; on the last frames
   sty   AUDV0            ; turn off sound
@nomute:
   cpx   #$50             ; see if we want "jumping" sound effect
   bcs   @disploop
   lda   #$06 
   sta   AUDC0
   txa
   ldx   #$50             ; for drawing, set it back
   sbc   #$2f
   bpl   @norev
   eor   #$1f             ; second half, tone goes down again
@norev:
   sta   AUDF0

@disploop:
   cpx   #$90
   tya
   bcc   @cleargfx
   lda   $00,x
@cleargfx:
   sta   WSYNC
   sta   PF2              ; set precalc'ed gfx data

   txa
   lsr
   lsr
   lsr
   and   #$0f
   tay
   txa
   asl
   and   #$0f
   ora   ntscpaltable,y
   ldy   #$00
   sta   COLUP0

   txa
   asl
   sta   COLUP1

   inx
   cpx   #$ee ; last byte = $00 to clear PF2
   bcc   @disploop

@exit:
   
   jsr   waitscreen
   jmp   waitoverscan
