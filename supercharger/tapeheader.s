
.include "globals.inc"

; from https://atariage.com/forums/topic/75440-supercharger-jumpstart-22/
; Supercharger 8448 bin format
; 6114 bytes  24 pages each 256 bytes
; 2048 bytes  padding (ROM pages)
;    8 bytes  game header
;             - start address LSB
;             - start address MSB
;             - RAM config byte (saved in $80)
;               D7-D5 write pulse delay
;               D4-D2 bank config
;                   000   3   ROM
;                   001   1   ROM
;                   010   3   1
;                   011   1   3
;                   100   3   ROM
;                   101   2   ROM
;                   110   3   2
;                   111   2   3
;                D1 write enable (1 enable, 0 disable)
;                D0 ROM power (0 power on, 1 power off)
;              - page count (number of non-blank pages)
;              - checksum (sum of all 8 game header bytes = $55)
;              - multi load index number
;              - progress counter LSB (page count)*256/21 - (MSB-1)*256
;              - progress counter MSB (page count)/21 + 1
;    8 bytes   padding
;   24 bytes   page number table
;              - page # * 4 + bank # - 1 (0<=page #<=7, 1<=bank #<=3)
;    8 bytes   padding
;   24 bytes   page checksum table
;              - sum of bytes in page + page number value + page checksum = $55
;  184 bytes   padding

.segment "TAPEHEAD"
   .word reset       ; start address
   .byte %00011101   ; initial bank configuration
   .byte $18         ; ram config
   .byte $00         ; checksum
   .byte $00         ; multiloadindex
   .word $0224       ; "curtaindelay" (typically $0224)

   .res  $08         ; padding
   
   .byte $00,$04,$08,$0c,$10,$14,$18,$1c       ; page numbers for bank 1
   .byte $01,$05,$09,$0d,$11,$15,$19,$1d       ; page numbers for bank 2
   .byte $02,$06,$0a,$0e,$12,$16,$1a,$1e       ; page numbers for bank 3

   .res  $18         ; padding

   .byte $55,$55,$55,$55,$55,$55,$55,$55
   .byte $55,$55,$55,$55,$55,$55,$55,$55
   .byte $55,$55,$55,$55,$55,$55,$55,$55

   ;.res $A8         ; will be done by linker

