
.define SCVIB  $F000
.define SCBIOS $F800
.define SCCTRL $FFF8
.define SCAUD  $FFF9

; Hotspots
; ========
; F000            ; SCVIB (SC Value Index Base)
; FFF8: DDDB BBWE ; SCCTRL (SC ConTRoL)
; FFF9: 0000 000A ; SCAUD (SC AUDio in)
; A: Supercharger audio data (0 = low input, 1 = high input)
; D: write delay (unused, set to 0)
; B: bankswitching mode (see below)
; W: RAM write enable (1 = enabled, 0 = disabled)
; E: ROM power enable (0 = enabled, 1 = turn off ROM)

; BBB    1000-17FF,  1800-1FFF
; ---    ---------------------
; 000 *  RAM bank 3  ROM
; 001 -  RAM bank 1  ROM
; 010 *  RAM bank 3  RAM bank 1
; 011 -  RAM bank 1  RAM bank 3
; 100 -  RAM bank 3  ROM
; 101 -  RAM bank 2  ROM
; 110 *  RAM bank 3  RAM bank 2
; 111 -  RAM bank 2  RAM bank 3
; * = supported for startup

.define SCBK1    %00001001
.define SCBK1W   %00001011
.define SCBK2    %00011001
.define SCBK2W   %00011011
.define SCBKR    %00000010

; Writing to RAM
; ==============
; First the program generates an access to address $f0xx to select the value
; to store in the latch. That value is then written from the latch to the
; fifth address bus value after the $f0xx access.
; 
; example without vector:
; $f100: ldx #$00
; $f102: ldy #$12
; $f104: cmp $f000,x ; <- triggers counting
; $f107: nop
; $f108: cmp $f000,y
; 1: $f107 ea ; nop instruction
; 2: $f108 d9 ; cmp abs,y instruction
; 3: $f109 00 ; cmp address low byte
; 4: $f10a f0 ; cmp address high byte
; 5: $f012 WRITE taking over cmp instruction
; 
; example with vector:
; $f100: ldx #$00
; $f102: ldy #$12
; $f104: cmp $f000,x ; <- triggers counting
; $f107: cmp ($80),y
; 1: $f107 d1 ; cmp (zp),y instruction
; 2: $f108 80 ; cmp zp vector address
; 3: $0080 00 ; zp vector low byte
; 4: $0081 f0 ; zp vector high byte
; 5: $f012 WRITE taking over cmp instruction
; 
