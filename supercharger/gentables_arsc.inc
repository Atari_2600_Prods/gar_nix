
.include "supercharger.inc"

; enable write mode
   bit   SCVIB+$1f    ; write config to latch: banks 1,2 write enabled, no power
   nop
   bit   SCCTRL       ; write latch data to configuration register
   ldy   #$00
   cmp   SCVIB    ; write $00 to latch
   nop
   cmp   logo1r,y ; write from latch to address
   cmp   SCVIB    ; write $00 to latch
   nop
   cmp   blackr,y ; write from latch to address
@colloop:
   tya
.if NTSC
   and   #$f0
   sta   tmp8
.else
   lsr
   lsr
   lsr
   lsr
   tax
.endif
   tya
   and   #$0f
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
.if NTSC
   ora   tmp8
.else
   ora   highs,x
.endif
   tax
   cmp   SCVIB,x  ; write value to latch
   nop
   cmp   colr,y   ; write from latch to address
   iny

   bne   @colloop

; sine table

   ldx   #$3f

; Accumulate the delta (normal 16-bit addition)
@sinloop:

   clc
   lda   delta+0
   adc   value+0
   sta   value+0
   lda   delta+1
   adc   value+1
   sta   value+1

; Reflect the value around for a sine wave
   txs
   tax
   cmp   SCVIB,x     ; write value to latch
   nop
   cmp   siner+$c0,y ; write from latch to address
   cmp   SCVIB,x     ; write value to latch
   tsx
   cmp   siner+$80,x ; write from latch to address
   eor   #$7f
   tax
   cmp   SCVIB,x     ; write value to latch
   nop
   cmp   siner+$40,y ; write from latch to address
   cmp   SCVIB,x     ; write value to latch
   tsx
   cmp   siner+$00,x ; write from latch to address

; Increase the delta, which creates the "acceleration" for a parabola
   clc
   lda   #$08
   adc   delta+0   ; this value adds up to the proper amplitude
   sta   delta+0
   bcc   :+
   inc   delta+1
:

; Loop
   iny
   dex
   bpl   @sinloop

   ldx   #$7f
   ldy   #$0b
@imgloop:

; Compact code: hijack loop for copying code
   cpx   #<(ramcodeend-ramcode)
   bcs   :+
   lda   ramcode,x
   sta   coderam,x
:

   cpy   #$08
   bcs   :+
   sty   tmp8
   lda   logo1,y
   tay
   cmp   SCVIB,y
   nop
   cmp   logo1r,x
   ldy   tmp8
   lda   logo2,y
   tay
   cmp   SCVIB,y
   nop
   cmp   logo2r,x
   ldy   tmp8
:
   txa
   and   #$07
   bne   :+
   dey
:
   dex
   bpl   @imgloop

; disable write mode
;   ldx   #$ff
   bit   SCVIB+$1d    ; write config to latch: banks 1,2 write disabled, no power
   txs
   bit   SCCTRL       ; write latch data to configuration register
